package com.example.utils;

public interface IPoliceProjection {
    int getIdPolice();
    String getName();
    String getSurname();
    String getPhone();
    String getEmail();
    String getCode();
    int getIsMajor();
    int getIsBusy();
    String getStatus();

    
}
