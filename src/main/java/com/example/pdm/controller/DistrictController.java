package com.example.pdm.controller;

import java.util.List;

import com.example.pdm.model.District;
import com.example.pdm.service.DistrictService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping({"/district"})
public class DistrictController {
    @Autowired
     private DistrictService serviceDistrict;

    @GetMapping
    public List<District> listAll(){
        return serviceDistrict.listAll();
    }

    @PostMapping
    public @ResponseBody Object save(@RequestBody District d){
        try {
            return serviceDistrict.save(d);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
        
    }

    @PutMapping("/{id}")
    public District edit(@RequestBody District b, @PathVariable("id") int id){
        b.setIdDistrict(id);
        return serviceDistrict.edit(b);
    }
    
}
