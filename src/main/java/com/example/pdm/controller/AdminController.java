package com.example.pdm.controller;

import com.example.pdm.model.Admin;
import com.example.pdm.service.AdminService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping({"/admin"})
public class AdminController {
    @Autowired
     private AdminService serviceAdmin;
     @PostMapping
     public @ResponseBody Object save(@RequestBody Admin d){
         try {
             d.setStatus("Active");
             return serviceAdmin.save(d);
         } catch (Exception e) {
             System.out.println(e.getMessage());
             return e.getMessage();
         }
     }
    @PutMapping("/{id}")
    public Admin edit(@RequestBody Admin b, @PathVariable("id") int id){
        b.setIdUser(id);
        return serviceAdmin.edit(b);
    }

    @GetMapping
    public @ResponseBody Object listAll(){
        return serviceAdmin.listAll();
    }
}
