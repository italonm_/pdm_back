package com.example.pdm.controller;

import com.example.pdm.model.Citizen;
import com.example.pdm.service.CitizenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping({"/citizen"})
public class CitizenController {
    @Autowired
     private CitizenService serviceCitizen;
     @PostMapping
     public @ResponseBody Object save(@RequestBody Citizen d){
         try {
             d.setStatus("Active");
             return serviceCitizen.save(d);
         } catch (Exception e) {
             System.out.println(e.getMessage());
             return e.getMessage();
         }
         
     }
    @PutMapping("/{id}")
    public Citizen edit(@RequestBody Citizen b, @PathVariable("id") int id){
        b.setIdUser(id);
        return serviceCitizen.edit(b);
    }

    @GetMapping
    public @ResponseBody Object listAll(){
        return serviceCitizen.listAll();
    }
}
