package com.example.pdm.controller;

import java.util.List;

import com.example.pdm.model.Police;
import com.example.pdm.service.PoliceService;
import com.example.utils.IPoliceProjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping({"/police"})
public class PoliceController {
    @Autowired
     private PoliceService servicePolice;

    @GetMapping
    public @ResponseBody Object listAll(){
        try {
            return servicePolice.listAll();
            
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
        
    }

    @PostMapping
    public @ResponseBody Object save(@RequestBody Police d){
        try {
            return servicePolice.save(d);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
        
    }

    @PutMapping("/{id}")
    public Police editar(@RequestBody Police b, @PathVariable("id") int id){
        b.setIdPolice(id);
        return servicePolice.edit(b);
    }
    
}
