package com.example.pdm.controller;

import java.util.List;

import com.example.pdm.model.IncidentType;
import com.example.pdm.service.IncidentTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping({"/incidentType"})
public class IncidentTypeController {
    @Autowired
     private IncidentTypeService serviceIncidentType;

     @GetMapping
     public List<IncidentType> listAll(){
         return serviceIncidentType.listAll();
     }
 
     @PostMapping
     public @ResponseBody Object save(@RequestBody IncidentType d){
         try {
             return serviceIncidentType.save(d);
         } catch (Exception e) {
             System.out.println(e.getMessage());
             return e.getMessage();
         }
         
     }
 
     @PutMapping("/{id}")
     public IncidentType edit(@RequestBody IncidentType b, @PathVariable("id") int id){
         b.setIdIncidentType(id);
         return serviceIncidentType.edit(b);
     }
}
