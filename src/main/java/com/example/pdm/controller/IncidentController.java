package com.example.pdm.controller;

import java.util.List;

import com.example.pdm.model.Incident;
import com.example.pdm.service.IncidentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping({"/incident"})
public class IncidentController {
    @Autowired
     private IncidentService serviceIncident;

    @GetMapping
    public List<Incident> listAll(){
        return serviceIncident.listAll();
    }

    @PostMapping
    public @ResponseBody Object save(@RequestBody Incident d){
        try {
            return serviceIncident.save(d);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return e.getMessage();
        }
        
    }

    @PutMapping("/{id}")
    public Incident edit(@RequestBody Incident b, @PathVariable("id") int id){
        b.setIdIncident(id);
        return serviceIncident.edit(b);
    }
    
}
