package com.example.pdm.service;
import java.util.List;

import com.example.pdm.interfaces.IPolice;
import com.example.pdm.model.Police;
import com.example.utils.IPoliceProjection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PoliceService {

    @Autowired
    private IPolice repo;

    public Police save(Police d) {
        d.setIsBusy(0);
        d.setStatus("Active");
        return repo.save(d);
    }

    public Police edit(Police d) {
        return repo.save(d);
    }

    public List<IPoliceProjection> listAll() {
        return repo.listPolices();
    }

}