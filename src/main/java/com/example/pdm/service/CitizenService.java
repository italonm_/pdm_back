package com.example.pdm.service;
import java.util.List;

import com.example.pdm.interfaces.ICitizen;
import com.example.pdm.model.Citizen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CitizenService {
    @Autowired
    private ICitizen repo;

    public Citizen save(Citizen d) {
        return repo.save(d);
    }

    public Citizen edit(Citizen d) {
        return repo.save(d);
    }
    public List<Citizen> listAll() {
        return repo.listAllCitizen();
    }
    
}
