package com.example.pdm.service;

import java.util.List;

import com.example.pdm.interfaces.IPhoto;
import com.example.pdm.model.Photo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhotoService {
    @Autowired
    private IPhoto repo;

    public Photo save(Photo d) {
        return repo.save(d);
    }

    public Photo edit(Photo d) {
        return repo.save(d);
    }

    
}
