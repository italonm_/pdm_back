package com.example.pdm.service;

import java.util.List;

import com.example.pdm.interfaces.IDistrict;
import com.example.pdm.model.District;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DistrictService {
    @Autowired
    private IDistrict repo;

    public District save(District d) {
        return repo.save(d);
    }

    public District edit(District d) {
        return repo.save(d);
    }

    public List<District> listAll() {
        return repo.findAllByOrderByName();
    }
    
}
