package com.example.pdm.service;

import java.util.List;

import com.example.pdm.interfaces.IIncident;
import com.example.pdm.model.Incident;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IncidentService {
    @Autowired
    private IIncident repo;

    public Incident save(Incident d) {
        return repo.save(d);
    }

    public Incident edit(Incident d) {
        return repo.save(d);
    }

    public List<Incident> listAll() {
        return repo.findAll();
    }
}