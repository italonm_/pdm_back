package com.example.pdm.service;
import java.util.List;

import com.example.pdm.interfaces.IAdmin;
import com.example.pdm.model.Admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {
    @Autowired
    private IAdmin repo;

    public Admin save(Admin d) {
        return repo.save(d);
    }

    public Admin edit(Admin d) {
        return repo.save(d);
    }

    public List<Admin> listAll() {
        return repo.listAllAdmin();
    }

    
}
