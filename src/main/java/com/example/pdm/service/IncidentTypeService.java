package com.example.pdm.service;

import java.util.List;

import com.example.pdm.interfaces.IIncidentType;
import com.example.pdm.model.IncidentType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IncidentTypeService {
    @Autowired
    private IIncidentType repo;

    public IncidentType save(IncidentType d) {
        return repo.save(d);
    }

    public IncidentType edit(IncidentType d) {
        return repo.save(d);
    }

    public List<IncidentType> listAll() {
        return repo.findAllByOrderByName();
    }

    
}
