package com.example.pdm.model;

import java.sql.Time;
import java.util.Date;
import java.util.Set;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Incident")
public class Incident {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idIncident;
    private String address;
    private double latitude;
    private double longitude;
    private String description;
    @ManyToOne
    @JoinColumn(name = "idIncidentType", referencedColumnName = "idIncidentType", nullable = true)
    private IncidentType incidentType;
    @OneToMany(mappedBy = "incident")
    private Set<Photo> photos;
    @ManyToOne
    @JoinColumn(name = "idDistrict", referencedColumnName = "idDistrict", nullable = true)
    private District district;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm", timezone = "America/Lima")
    @Column(columnDefinition="DATETIME")
    private Date date;
    @ManyToOne
    @JoinColumn(name = "idPolice", referencedColumnName = "idPolice", nullable = true)
    private Police policeMinor;
    @ManyToOne
    @JoinColumn(name = "idCitizen", referencedColumnName = "idCitizen", nullable = true)
    private Citizen citizen;
    private String status;

    

    public int getIdIncident() {
        return idIncident;
    }

    public void setIdIncident(int idIncident) {
        this.idIncident = idIncident;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IncidentType getIncidentType() {
        return incidentType;
    }

    public void setIncidentType(IncidentType incidentType) {
        this.incidentType = incidentType;
    }

    public Set<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(Set<Photo> photos) {
        this.photos = photos;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public Police getPoliceMinor() {
        return policeMinor;
    }

    public void setPoliceMinor(Police policeMinor) {
        this.policeMinor = policeMinor;
    }

    public Citizen getCitizen() {
        return citizen;
    }

    public void setCitizen(Citizen citizen) {
        this.citizen = citizen;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Incident(int idIncident, String address, double latitude, double longitude, String description,
            IncidentType incidentType, Set<Photo> photos, District district, Date date, Time hour, Police policeMinor,
            Citizen citizen, String status) {
        this.idIncident = idIncident;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.description = description;
        this.incidentType = incidentType;
        this.photos = photos;
        this.district = district;
        this.date = date;
        this.policeMinor = policeMinor;
        this.citizen = citizen;
        this.status = status;
    }

    public Incident() {
    }
    
    
}
