package com.example.pdm.model;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "Admin")
@PrimaryKeyJoinColumn(name="idAdmin")
public class Admin extends User{

    public Admin(int idUser, String username, String password, String name, String surname, String phone, String email,
            String status) {
        super(idUser, username, password, name, surname, phone, email, status);
    }

    public Admin() {
    }
    
    
}
