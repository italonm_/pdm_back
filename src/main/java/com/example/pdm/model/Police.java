package com.example.pdm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@Table(name = "Police")
public class Police {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPolice;
    private String password;
    @Column(length = 50)
    private String name;
    @Column(length = 50)
    private String surname;
    @Column(length = 10)
    private String phone;
    @Column(length = 50)
    private String email;
    private String code;
    private int isMajor;
    private int isBusy;
    @Column(length = 50)
    private String status;

    public int getIdPolice() {
        return idPolice;
    }

    public void setIdPolice(int idPolice) {
        this.idPolice = idPolice;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getIsMajor() {
        return isMajor;
    }

    public void setIsMajor(int isMajor) {
        this.isMajor = isMajor;
    }

    public int getIsBusy() {
        return isBusy;
    }

    public void setIsBusy(int isBusy) {
        this.isBusy = isBusy;
    }

    public Police(int idPolice, String password, String name, String surname, String phone, String email, String code,
            int isMajor, int isBusy,String status) {
        this.idPolice = idPolice;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.code = code;
        this.isMajor = isMajor;
        this.isBusy = isBusy;
        this.status = status;
        
    }

    public Police() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
