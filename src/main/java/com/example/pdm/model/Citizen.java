package com.example.pdm.model;


import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@Table(name = "Citizen")
@PrimaryKeyJoinColumn(name="idCitizen")
public class Citizen extends User{

    public Citizen(int idUser, String username, String password, String name, String surname, String phone,
            String email, String status) {
        super(idUser, username, password, name, surname, phone, email, status);
    }

    public Citizen() {
    }
    
}
