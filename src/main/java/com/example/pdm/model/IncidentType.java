package com.example.pdm.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Table(name = "IncidentType")
public class IncidentType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idIncidentType;
    private String name;
    private String description;

    public IncidentType(int idIncidentType, String name, String description) {
        this.idIncidentType = idIncidentType;
        this.name = name;
        this.description = description;
    }

    public int getIdIncidentType() {
        return idIncidentType;
    }

    public void setIdIncidentType(int idIncidentType) {
        this.idIncidentType = idIncidentType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IncidentType() {
    }

}
