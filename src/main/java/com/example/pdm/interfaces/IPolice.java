package com.example.pdm.interfaces;

import java.util.List;

import com.example.pdm.model.Police;
import com.example.utils.IPoliceProjection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IPolice extends JpaRepository<Police, Integer> {
    
    @Query("SELECT p FROM Police p")
    public List<IPoliceProjection> listPolices();
    
}
