package com.example.pdm.interfaces;

import java.util.List;

import com.example.pdm.model.Citizen;

import org.springframework.data.jpa.repository.Query;

public interface ICitizen extends IUser {

      //"SELECT c FROM Cronograma c join fetch c.beneficiario  WHERE c.fechaIni >=?1 AND 
    //c.fechaIni <?2 AND c.lugarCobro.idLugarCobro = ?3 order by c.fechaIni")
    @Query("SELECT c FROM Citizen c ")
    public List<Citizen> listAllCitizen();
    
}
