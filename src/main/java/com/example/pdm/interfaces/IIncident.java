package com.example.pdm.interfaces;

import java.util.List;

import com.example.pdm.model.Incident;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IIncident extends JpaRepository<Incident, Integer>  {
    
}
