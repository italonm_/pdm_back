package com.example.pdm.interfaces;

import java.util.List;

import com.example.pdm.model.District;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDistrict extends JpaRepository<District, Integer> {
    
    public List<District> findAllByOrderByName();
    
}
