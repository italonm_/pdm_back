package com.example.pdm.interfaces;

import java.util.List;

import com.example.pdm.model.Admin;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IAdmin extends IUser {

    @Query("SELECT a FROM Admin a ")
    public List<Admin> listAllAdmin();
    
}
