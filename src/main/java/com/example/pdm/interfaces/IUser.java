package com.example.pdm.interfaces;

import java.util.List;

import com.example.pdm.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IUser extends JpaRepository<User,Integer> {
  
}
