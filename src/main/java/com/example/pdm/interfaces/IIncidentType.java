package com.example.pdm.interfaces;

import java.util.List;

import com.example.pdm.model.IncidentType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IIncidentType extends JpaRepository<IncidentType, Integer> {

    public List<IncidentType> findAllByOrderByName();
    
}
