package com.example.pdm.interfaces;

import com.example.pdm.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPhoto extends JpaRepository<Photo, Integer>  {
    
}
